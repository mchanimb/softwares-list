# SOFTWARE LIST

## Utils

- Acrobat Reader
- VLC
- Firefox
  - uBlock Origin
  - Bitwarden 
- WinSCP / PuTTY / FileZilla
- CrystalDiskInfo
- Transmission
- Sumatra PDF

## Entertainment

- Discord
- Plex
- Spotify
- Steam / Battle.net / GOG.com / Uplay / Origin

## Work

- VSCode
  - Prettier
  - Remote SSH
  - Path Intellisense
  - Git Lens
  - EditorConfig
  - ESLint / TSLint
  - WSL
- Insomnia / Postman
- IntelliJ
- Git / GitKraken / Sourcetree
- Dbeaver
- Ubuntu / Docker
